# Instance list

This repo contains a list of known public instances.

## Users

- `hyperpipe.codeberg.page/#/$PATH`

where, `$PATH` (optional) could be any path compatable with Hyperpipe

## New Submissions

Firstly, Thank you for hosting Hyperpipe 🙇

To add a new instance to the list please [create a PR](https://codeberg.org/Hyperpipe/pages/pulls)

- Add the frontend and backend details to their respective files in [`/api`](https://codeberg.org/Hyperpipe/pages/src/branch/main/api)

- New instances must be added in the order `clearnet` -> `onion` -> `others`

- Modifications to the frontend/backend code (AGPLv3) should be linked in the PR

- While waiting for your PR to be merged, please confirm that your instance is up-to-date and is configured properly