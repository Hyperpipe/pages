# Instances

## Frontends

| Instance | Privacy |
| :------: | :-----: |
| [hyperpipe.surge.sh (Official)](https://hyperpipe.surge.sh) | https://codeberg.org/Hyperpipe/Hyperpipe/wiki/Privacy |
| [music.adminforge.de](https://music.adminforge.de) | https://adminforge.de/datenschutz/ |
| [music.pfcd.me](https://music.pfcd.me) | https://codeberg.org/pfc/pfcdme/src/branch/main/privacy.md |
| [hyperpipe.frontendfriendly.xyz](https://hyperpipe.frontendfriendly.xyz) | https://frontendfriendly.xyz/privacy.html |
| [hyperpipe.drgns.space](https://hyperpipe.drgns.space) | https://codeberg.org/dragongoose/privacy/src/branch/main/README.md |
| [hyperpipe.projectsegfau.lt](https://hyperpipe.projectsegfau.lt) | https://projectsegfau.lt/legal/privacy-policy |
| [hp.ggtyler.dev](https://hp.ggtyler.dev) | https://www.ggtyler.dev/other/privacy |
| [hyperpipe.lunar.icu](https://hyperpipe.lunar.icu) | https://service.lunar.icu/LunarPrivacy.pdf |
| [hp.iqbalrifai.eu.org](https://hp.iqbalrifai.eu.org) | https://iqbalrifai.eu.org/privacy-policy/ |
| [hp.ngn.tf](https://hp.ngn.tf) | https://files.ngn.tf/privacy |
| [hyperpipe.ducks.party](https://hyperpipe.ducks.party) | https://ducks.party/privacy |
| [hyperpipe.darkness.services](https://hyperpipe.darkness.services) | https://codeberg.org/Hyperpipe/Hyperpipe/wiki/Privacy.md |

## Backends

| Instance | Locations |
| :------: | :-------: |
| [hyperpipeapi.onrender.com (Official)](https://hyperpipeapi.onrender.com) | 🇩🇪 |
| [musicapi.adminforge.de](https://musicapi.adminforge.de) | 🇩🇪 |
| [hyperpipeapi.pfcd.me](https://hyperpipeapi.pfcd.me) | 🇩🇪 |
| [hyperpipeapi.frontendfriendly.xyz](https://hyperpipeapi.frontendfriendly.xyz) | 🇺🇸 |
| [hyperpipeapi.drgns.space](https://hyperpipeapi.drgns.space) | 🇺🇸 |
| [hyperpipebackend.us.projectsegfau.lt](https://hyperpipebackend.us.projectsegfau.lt) | 🇺🇸 |
| [hyperpipebackend.eu.projectsegfau.lt](https://hyperpipebackend.eu.projectsegfau.lt) | 🇩🇪 |
| [hyperpipebackend.in.projectsegfau.lt](https://hyperpipebackend.in.projectsegfau.lt) | 🇮🇳 |
| [hpapi.ggtyler.dev](https://hpapi.ggtyler.dev) | 🇺🇸 |
| [hyperpipe-api.lunar.icu](https://hyperpipe-api.lunar.icu) | 🇩🇪 |
| [hp-proxy.iqbalrifai.eu.org](https://hp-proxy.iqbalrifai.eu.org) | 🇮🇩 |
| [hpapi.ngn.tf](https://hpapi.ngn.tf) | 🇹🇷 |
| [hyperpipeapi.ducks.party](https://hyperpipeapi.ducks.party) | 🇳🇱 |
| [hyperpipeapi.darkness.services](https://hyperpipeapi.darkness.services) | 🇺🇸 |
