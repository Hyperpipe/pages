#!/bin/sh

set -e

FRONTENDS=$(cat ./api/frontend.json | jq .[])
BACKENDS=$(cat ./api/backend.json | jq .[])

mkdir -p ./gen

## CSV

echo "Name,Url,Privacy Policy" > ./gen/frontend.csv
echo $FRONTENDS | jq -r '.name+","+.url+","+.privacy' >> ./gen/frontend.csv

echo "Name,Locations,Url,Org" > ./gen/backend.csv
echo $BACKENDS | jq -r '.name+","+.locations+","+.api_url+","+.org' >> ./gen/backend.csv

## MD

push() {
	echo $1 >> INSTANCES.md
}

echo "# Instances" > INSTANCES.md
push ""
push "## Frontends"
push ""
push "| Instance | Privacy |"
push "| :------: | :-----: |"
echo $FRONTENDS | jq -r '"| ["+.name+"]("+.url+") | "+.privacy+" |"' >> INSTANCES.md
push ""
push "## Backends"
push ""
push "| Instance | Locations |"
push "| :------: | :-------: |"
echo $BACKENDS | jq -r '"| ["+.name+"]("+.api_url+") | "+.locations+" |"' >> INSTANCES.md